import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  token: null | string = null;

  constructor(private auth: AuthService) {
    this.auth.token$.subscribe(token => {
      this.token = token;
    });
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (request.url.includes('/user')) {
      // Inject my token here
      (request.body as any).token = this.token;
    }
    return next.handle(request);
  }
}
