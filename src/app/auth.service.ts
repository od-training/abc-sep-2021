import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token$ = new BehaviorSubject(null);

  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    this.http.post('http://localhost:3000/auth', { username, password })
      .subscribe(({ status, token }: any) => {
        if (status === 'success') {
          this.token$.next(token);
        }
      });
  }

  getUser() {
    return this.http.post('http://localhost:3000/user', {})
  }

}
