import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user$ = new Observable();

  constructor(private auth: AuthService) {
    this.auth.token$.subscribe(token => {
      console.log('My Token is : ', token);
    });

    this.getUser();
  }

  ngOnInit(): void { }

  login() {
    // Grab the username and password from a form
    const username = 'test';
    const password = 'test';
    this.auth.login(username, password);

    setTimeout(() => {
      this.getUser();
    }, 2500);
  }

  getUser() {
    this.user$ = this.auth.getUser();
  }

}
