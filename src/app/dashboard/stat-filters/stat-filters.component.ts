import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent implements OnInit {

  numbers$!: Observable<any>;
  page: number = 1;

  constructor(private http: HttpClient) {
    this.fetchNumbers();
  }

  ngOnInit(): void {
  }

  fetchNumbers() {
    this.numbers$ = this.http.get<any>('http://localhost:3000/pagination', { params: { page: this.page } });
  }

  previousPage() {
    if (this.page - 1 <= 0) return;

    this.page -= 1;

    this.fetchNumbers();
  }

  nextPage() {
    this.page += 1;

    this.fetchNumbers();
  }


  /* Example API Endpoint

  @Get('pagination')
  getPagination(@Query('limit') limit = 30, @Query('page') page = 1) {
    const paginationData = Array.from(Array(1000).keys());
    const startingInt = (page * limit) - limit;
    const data = paginationData.slice(startingInt, startingInt + limit);

    return {
      count: paginationData.length,
      data
    };
  }

  */

}
