import { Component, Input, OnInit } from '@angular/core';
import { Video } from 'src/app/types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[] = [];
  @Input() selectedVideo: string | undefined;

  constructor() { }

  ngOnInit(): void { }

}
