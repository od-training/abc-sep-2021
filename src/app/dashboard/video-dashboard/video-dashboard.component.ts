import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from 'src/app/types';
import { ActivatedRoute } from '@angular/router';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent implements OnInit {
  selectedVideo: Observable<Video>;
  videos: Observable<Video[]>;

  constructor(private videoService: VideoDataService, route: ActivatedRoute) {
    this.videos = this.videoService.loadVideos();
    this.selectedVideo = route.queryParamMap.pipe(
      map((params) => params.get('videoId') as string),
      filter((id) => !!id),
      switchMap((id) => videoService.loadSingleVideo(id))
    );
  }

  ngOnInit(): void {}
}
