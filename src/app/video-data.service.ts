import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from './types';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) {}

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos');
  }

  loadSingleVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`https://api.angularbootcamp.com/videos/${id}`);
  }
}
